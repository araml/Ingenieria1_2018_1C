!classDefinition: #ShoppingCartTest category: #'TusLibros.com'!
TestCase subclass: #ShoppingCartTest
	instanceVariableNames: 'cart aBook anotherBook yetAnotherBook notInCatalogueBook catalogue'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros.com'!

!ShoppingCartTest methodsFor: 'as yet unclassified' stamp: 'hjgg 6/7/2018 15:17:23'!
setUp

	| catalogue |
	
	aBook _ Book new.
	anotherBook _ Book new.
	yetAnotherBook _ Book new.
	notInCatalogueBook _ Book new.
	catalogue _ Set new add: aBook; add: anotherBook; add: yetAnotherBook; yourself.
	cart _ ShoppingCart withCatalogue: catalogue.! !

!ShoppingCartTest methodsFor: 'as yet unclassified' stamp: 'hjgg 6/6/2018 23:44:18'!
test01CartIsCreatedEmpty

	self assert: cart isEmpty.! !

!ShoppingCartTest methodsFor: 'as yet unclassified' stamp: 'hjgg 6/7/2018 13:04:55'!
test02CartWithOneBookShouldNotBeEmpty

	cart add: aBook.
	self deny: cart isEmpty.! !

!ShoppingCartTest methodsFor: 'as yet unclassified' stamp: 'hjgg 6/7/2018 13:05:10'!
test03CartWithManyCopiesOfSameBookShouldNotBeEmpty

	| copies |
	
	copies _ 3.
	cart add: aBook withCopies: copies.
	self deny: cart isEmpty.! !

!ShoppingCartTest methodsFor: 'as yet unclassified' stamp: 'hjgg 6/7/2018 13:08:01'!
test04CartWithoutBookShouldFailUponRemovingIt
	self
		should: [ cart remove: aBook ]
		raise: Error
		withExceptionDo: [ :error |
			self assert: error messageText = 'Object is not in the collection.' ].! !

!ShoppingCartTest methodsFor: 'as yet unclassified' stamp: 'hjgg 6/7/2018 13:08:56'!
test05RemovingMoreThanAvailableCopiesShouldFail
	cart
		add: aBook
		withCopies: 2.
	self
		should: [
			cart
				remove: aBook
				copies: 3 ]
		raise: Error
		withExceptionDo: [ :error |
			self assert: error messageText = 'Object is not in the collection.' ].! !

!ShoppingCartTest methodsFor: 'as yet unclassified' stamp: 'hjgg 6/7/2018 15:19:46'!
test06ListingBooksShouldShowAllAddedBooks
	
	| bookList |
	
	cart add: aBook withCopies: 3.
	cart add: anotherBook withCopies: 2.
	cart add: yetAnotherBook.
	bookList _ cart listBooks.
	
	self assert: ((bookList includesKey: aBook) and: ((bookList at: aBook) = 3)).
	self assert: ((bookList includesKey: anotherBook) and: ((bookList at: anotherBook) = 2)).
	self assert: ((bookList includesKey: yetAnotherBook) and: ((bookList at: yetAnotherBook) = 1)).! !

!ShoppingCartTest methodsFor: 'as yet unclassified' stamp: 'hjgg 6/7/2018 15:23:40'!
test07AddingBookNotInCatalogueShouldFailAndRemainEmpty

	self should: [cart add: notInCatalogueBook] 
			raise: Error 
				withExceptionDo: [:error | 
					self assert: error messageText = ShoppingCart bookNotAvailableInCatalogueErrorMessage.
					self assert: cart isEmpty.]! !


!classDefinition: #Book category: #'TusLibros.com'!
Object subclass: #Book
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros.com'!


!classDefinition: #ShoppingCart category: #'TusLibros.com'!
Object subclass: #ShoppingCart
	instanceVariableNames: 'cart catalogue'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros.com'!

!ShoppingCart methodsFor: 'testing' stamp: 'hjgg 6/6/2018 20:51:02'!
isEmpty
	^cart isEmpty.! !


!ShoppingCart methodsFor: 'initialization' stamp: 'hjgg 6/7/2018 15:03:56'!
initializeWithCatalogue: aCatalogue
	catalogue _ aCatalogue.
	cart _ Bag new.! !


!ShoppingCart methodsFor: 'adding' stamp: 'hjgg 6/7/2018 15:18:19'!
add: aBook 
	
	(catalogue includes: aBook) ifFalse: [self error: self class bookNotAvailableInCatalogueErrorMessage].
	cart add: aBook.! !

!ShoppingCart methodsFor: 'adding' stamp: 'hjgg 6/7/2018 15:18:32'!
add: aBook withCopies: copies 
	
	(catalogue includes: aBook) ifFalse: [self error: self class bookNotAvailableInCatalogueErrorMessage].
	cart add: aBook withOccurrences: copies.
	! !


!ShoppingCart methodsFor: 'removing' stamp: 'hjgg 6/7/2018 00:17:13'!
remove: aBook 
	cart remove: aBook.! !

!ShoppingCart methodsFor: 'removing' stamp: 'hjgg 6/7/2018 00:36:00'!
remove: aBook copies: numberOfCopies

	numberOfCopies timesRepeat: [cart remove: aBook].! !


!ShoppingCart methodsFor: 'listing' stamp: 'hjgg 6/7/2018 13:28:07'!
listBooks
	^cart contents.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'ShoppingCart class' category: #'TusLibros.com'!
ShoppingCart class
	instanceVariableNames: ''!

!ShoppingCart class methodsFor: 'as yet unclassified' stamp: 'hjgg 6/7/2018 15:13:09'!
bookNotAvailableInCatalogueErrorMessage
	
	^'Book not available in catalogue'.! !


!ShoppingCart class methodsFor: 'instance creation' stamp: 'hjgg 6/7/2018 15:06:05'!
withCatalogue: aCatalogue

	^self new initializeWithCatalogue: aCatalogue.! !
