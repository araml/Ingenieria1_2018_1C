!classDefinition: #TicTacToeTest category: #TicTacToStep21!
TestCase subclass: #TicTacToeTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TicTacToStep21'!

!TicTacToeTest methodsFor: 'as yet unclassified' stamp: 'jg 5/22/2018 09:59:04'!
test01

	|game|
	
	game := TicTacToe new.
	
	self assert: game Xs isEmpty.
	self assert: game Os isEmpty.
	! !

!TicTacToeTest methodsFor: 'as yet unclassified' stamp: 'jg 5/22/2018 10:20:33'!
test02

	|game|
	
	game := TicTacToe new.

	game putXAt: 1@1.
	
	self assert: 1 equals: game Xs size.
	self assert: (game Xs includes: 1@1).
	self assert: game Os isEmpty.
	
	! !

!TicTacToeTest methodsFor: 'as yet unclassified' stamp: 'jg 5/22/2018 10:50:55'!
test03

	|game|
	
	game := TicTacToe new.

	game putXAt: 1@1.
	game putOAt: 2@2.
	
	self assert: 1 equals: game Xs size.
	self assert: (game Xs includes: 1@1).
	self assert: 1 equals: game Os size.
	self assert: (game Os includes: 2@2).
	! !

!TicTacToeTest methodsFor: 'as yet unclassified' stamp: 'jg 5/22/2018 11:01:26'!
test04

	|game|
	
	game := TicTacToe new.

	game putXAt: 1@1.
	
	self
	should: [game putXAt: 2@2]
	raise: Error - MessageNotUnderstood
	withExceptionDo: [ :anError|
		self assert: TicTacToe notXTurnErrorMessage equals: anError messageText.	
		self assert: 1 equals: game Xs size.
		self assert: (game Xs includes: 1@1).
		self assert: game Os isEmpty.
		].
	! !

!TicTacToeTest methodsFor: 'as yet unclassified' stamp: 'jg 5/22/2018 11:08:58'!
test05

	|game|
	
	game := TicTacToe new.

	game putXAt: 1@1.
	game putOAt: 2@2.
	self
	should: [game putOAt: 3@3]
	raise: Error - MessageNotUnderstood
	withExceptionDo: [ :anError|
		self assert: TicTacToe notXTurnErrorMessage equals: anError messageText.	
		self assert: 1 equals: game Xs size.
		self assert: (game Xs includes: 1@1).
		self assert: 1 equals: game Os size.
		self assert: (game Os includes: 2@2).
		].
	! !

!TicTacToeTest methodsFor: 'as yet unclassified' stamp: 'jg 5/22/2018 11:18:20'!
test06

	|game|
	
	game := TicTacToe new.

	game putXAt: 1@1.
	game putOAt: 2@2.
	self
	should: [game putXAt: 1@1]
	raise: Error - MessageNotUnderstood
	withExceptionDo: [ :anError|
		self assert: TicTacToe positionTakenErrorMessage equals: anError messageText.	
		self assert: 1 equals: game Xs size.
		self assert: (game Xs includes: 1@1).
		self assert: 1 equals: game Os size.
		self assert: (game Os includes: 2@2).
		].
	! !

!TicTacToeTest methodsFor: 'as yet unclassified' stamp: 'jg 5/22/2018 11:37:38'!
test07

	|game|
	
	game := TicTacToe new.

	game putXAt: 1@1.
	game putOAt: 2@2.
	self
	should: [game putXAt: 2@2]
	raise: Error - MessageNotUnderstood
	withExceptionDo: [ :anError|
		self assert: TicTacToe positionTakenErrorMessage equals: anError messageText.	
		self assert: 1 equals: game Xs size.
		self assert: (game Xs includes: 1@1).
		self assert: 1 equals: game Os size.
		self assert: (game Os includes: 2@2).
		].
	! !

!TicTacToeTest methodsFor: 'as yet unclassified' stamp: 'jg 5/22/2018 11:45:04'!
test08

	|game|
	
	game := TicTacToe new.

	game putXAt: 1@1.
	self
	should: [game putOAt: 1@1]
	raise: Error - MessageNotUnderstood
	withExceptionDo: [ :anError|
		self assert: TicTacToe positionTakenErrorMessage equals: anError messageText.	
		self assert: 1 equals: game Xs size.
		self assert: (game Xs includes: 1@1).
		self assert:  game Os isEmpty.

		].
	! !

!TicTacToeTest methodsFor: 'as yet unclassified' stamp: 'jg 5/22/2018 12:12:12'!
test09

	|game|
	
	game := TicTacToe new.

	game putXAt: 1@1.
	game putOAt: 2@2.
	game putXAt: 3@3.
	self
	should: [game putOAt: 2@2]
	raise: Error - MessageNotUnderstood
	withExceptionDo: [ :anError|
		self assert: TicTacToe positionTakenErrorMessage equals: anError messageText.	
		self assert: 2 equals: game Xs size.
		self assert: (game Xs includes: 1@1).
		self assert: (game Xs includes: 3@3).
		self assert: 1 equals: game Os size.
		self assert: (game Os includes: 2@2).
		
		].
	! !

!TicTacToeTest methodsFor: 'as yet unclassified' stamp: 'jg 5/22/2018 12:24:06'!
test10

	|game|
	
	game := TicTacToe new.

	game putXAt: 1@1.
	game putOAt: 2@1.
	game putXAt: 2@2.
	game putOAt: 3@3.
	
		self deny: game OHasWon.
		self deny: game XHasWon.! !

!TicTacToeTest methodsFor: 'as yet unclassified' stamp: 'jg 5/22/2018 12:41:54'!
test11

	|game|
	
	game := TicTacToe new.

	game putXAt: 1@1.
	game putOAt: 2@1.
	game putXAt: 1@2.
	game putOAt: 3@3.
	game putXAt: 1@3.
	
	
		self assert: game XHasWon.
		self deny: game OHasWon.! !

!TicTacToeTest methodsFor: 'as yet unclassified' stamp: 'jg 5/22/2018 12:42:33'!
test12

	|game|
	
	game := TicTacToe new.

	game putXAt: 2@1.
	game putOAt: 1@1.
	game putXAt: 2@2.
	game putOAt: 3@1.
	game putXAt: 2@3.
	
	
		self assert: game XHasWon.
		self deny: game OHasWon.! !

!TicTacToeTest methodsFor: 'as yet unclassified' stamp: 'jg 5/22/2018 12:46:05'!
test13

	|game|
	
	game := TicTacToe new.

	game putXAt: 3@1.
	game putOAt: 1@1.
	game putXAt: 3@2.
	game putOAt: 2@1.
	game putXAt: 3@3.
	
	
		self assert: game XHasWon.
		self deny: game OHasWon.! !

!TicTacToeTest methodsFor: 'as yet unclassified' stamp: 'jg 5/22/2018 12:52:28'!
test14

	|game|
	
	game := TicTacToe new.

	game putXAt: 1@1.
	game putOAt: 2@2.
	game putXAt: 2@1.
	game putOAt: 3@3.
	game putXAt: 3@1.
	
	
		self assert: game XHasWon.
		self deny: game OHasWon.! !

!TicTacToeTest methodsFor: 'as yet unclassified' stamp: 'jg 5/22/2018 13:03:39'!
test15

	|game|
	
	game := TicTacToe new.

	game putXAt: 1@1.
	game putOAt: 2@1.
	game putXAt: 2@2.
	game putOAt: 2@3.
	game putXAt: 3@3.
	
	
		self assert: game XHasWon.
		self deny: game OHasWon.! !

!TicTacToeTest methodsFor: 'as yet unclassified' stamp: 'jg 5/22/2018 13:04:20'!
test16

	|game|
	
	game := TicTacToe new.

	game putXAt: 3@1.
	game putOAt: 2@1.
	game putXAt: 2@2.
	game putOAt: 1@2.
	game putXAt: 1@3.
	
	
		self assert: game XHasWon.
		self deny: game OHasWon.! !

!TicTacToeTest methodsFor: 'as yet unclassified' stamp: 'jg 5/22/2018 13:07:03'!
test17

	|game|
	
	game := TicTacToe new.

	game putXAt: 3@1.
	game putOAt: 1@1.
	game putXAt: 2@2.
	game putOAt: 1@2.
	game putXAt: 3@3.
	game putOAt: 1@3.
	
		self assert: game OHasWon.
		self deny: game XHasWon.! !

!TicTacToeTest methodsFor: 'as yet unclassified' stamp: 'jg 5/22/2018 13:15:12'!
test18

	| game |
	
	game := TicTacToe new.
	
	game putXAt: 1@1.	
	game putOAt: 2@1.
	game putXAt: 1@2.
	game putOAt: 3@1.	
	game putXAt: 1@3.

	self 
		should: [ game putOAt: 3@1 ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: TicTacToe canNotPlayWhenGameIsOverErrorMessage equals: anError messageText.
			self assert: 2 equals: game Os size.
			self assert: (game Os includes: 2@1).
			self assert: (game Os includes: 3@1) ]
			
			! !

!TicTacToeTest methodsFor: 'as yet unclassified' stamp: 'jg 5/22/2018 13:22:51'!
test19

	| game |
	
	game := TicTacToe new.
	
	game putXAt: 2@2.
	game putOAt: 1@1.	
	game putXAt: 2@1.
	game putOAt: 1@2.
	game putXAt: 3@1.	
	game putOAt: 1@3.

	self 
		should: [ game putXAt: 3@2 ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: TicTacToe canNotPlayWhenGameIsOverErrorMessage equals: anError messageText.
			self assert: 3 equals: game Xs size.
			self assert: (game Xs includes: 2@2).
			self assert: (game Xs includes: 2@1).
			self assert: (game Xs includes: 3@1) ]
			
			! !

!TicTacToeTest methodsFor: 'as yet unclassified' stamp: 'jg 5/22/2018 13:25:55'!
test20

	| game |
	
	game := TicTacToe new.
	
	game putXAt: 2@2.
	game putOAt: 1@1.	
	game putXAt: 1@3.
	game putOAt: 3@1.
	game putXAt: 2@1.	
	game putOAt: 2@3.
	game putXAt: 1@2.	
	game putOAt: 3@2.
	game putXAt: 3@3.
	
	self assert: game isOver.
	self deny: game XHasWon. 
	self deny: game OHasWon. 
	self assert: game isTied
! !

!TicTacToeTest methodsFor: 'as yet unclassified' stamp: 'jg 5/22/2018 15:10:03'!
test21

	| game |
	
	game := TicTacToe new.
	
	game putXAt: 2@2.
	game putOAt: 1@2.	
	game putXAt: 1@1.
	game putOAt: 2@1.
	game putXAt: 1@3.	
	game putOAt: 2@3.
	game putXAt: 3@2.	
	game putOAt: 3@1.
	game putXAt: 3@3.
	
	self assert: game isOver.
	self assert: game XHasWon. 
	self deny: game OHasWon. 
	self deny: game isTied
! !

!TicTacToeTest methodsFor: 'as yet unclassified' stamp: 'LR 5/30/2018 21:26:48'!
testIsPlayingX

	| game |
	
	game := TicTacToe new.
	
	game putXAt: 2@2.
	self assert: false = game isPlayingX.
	game putOAt: 1@2.	
	self assert: game isPlayingX.! !

!TicTacToeTest methodsFor: 'as yet unclassified' stamp: 'LR 5/30/2018 21:26:55'!
testIsPlayingY

	| game |
	
	game := TicTacToe new.
	
	game putXAt: 2@2.
	self assert: game isPlayingO.
	game putOAt: 1@2.	
	self assert: false = game isPlayingO.! !


!classDefinition: #TicTacToe category: #TicTacToStep21!
Object subclass: #TicTacToe
	instanceVariableNames: 'os xs turn turnState gameStage'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TicTacToStep21'!

!TicTacToe methodsFor: 'as yet unclassified' stamp: 'hh 5/31/2018 16:25:19'!
OHasWon
	^ gameStage OHasWon.! !

!TicTacToe methodsFor: 'as yet unclassified' stamp: 'jg 5/22/2018 10:01:34'!
Os

	^os copy.! !

!TicTacToe methodsFor: 'as yet unclassified' stamp: 'hh 5/31/2018 16:25:19'!
XHasWon
	^ gameStage XHasWon.! !

!TicTacToe methodsFor: 'as yet unclassified' stamp: 'jg 5/22/2018 10:01:25'!
Xs

	^xs copy.! !

!TicTacToe methodsFor: 'as yet unclassified' stamp: 'hh 5/31/2018 15:42:31'!
errorIfOccupied: aPosition.

	( (xs includes: aPosition) or: [os includes: aPosition] ) ifTrue: [ self error: self class positionTakenErrorMessage ].! !

!TicTacToe methodsFor: 'as yet unclassified' stamp: 'jg 5/22/2018 12:51:23'!
hasCompletedColumn: positions
	
	^ (1 to: 3) anySatisfy: [ :y | (positions count: [ :position | position y = y ]) = 3 ]! !

!TicTacToe methodsFor: 'as yet unclassified' stamp: 'jg 5/22/2018 12:36:01'!
hasCompletedRow: positions

	^ (1 to: 3) anySatisfy: [ :x | (positions count: [ :posicion | posicion x = x ]) = 3 ]! !

!TicTacToe methodsFor: 'as yet unclassified' stamp: 'jg 5/22/2018 12:56:21'!
hasDownDiagonal: positions

	^(1 to: 3) allSatisfy: [ :n | positions includes: n@n ]
! !

!TicTacToe methodsFor: 'as yet unclassified' stamp: 'jg 5/22/2018 13:02:13'!
hasUpDiagonal: positions

	^(1 to: 3) allSatisfy: [ :n | positions includes: n@(4-n) ]! !

!TicTacToe methodsFor: 'as yet unclassified' stamp: 'jg 5/22/2018 13:01:53'!
hasWin: positions

	^(self hasCompletedRow: positions)
		or: [(self hasCompletedColumn: positions) 
				or: [(self hasDownDiagonal: positions)
					or: [(self hasUpDiagonal: positions)]]]


! !

!TicTacToe methodsFor: 'as yet unclassified' stamp: 'hh 5/31/2018 16:26:57'!
initialize
	xs _ Set new.
	os _ Set new.
	turnState _ TicTacToeXTurn new.
	gameStage _ TicTacToeNotOver new.
	turn _ #x.! !

!TicTacToe methodsFor: 'as yet unclassified' stamp: 'hh 5/31/2018 16:50:57'!
isOver
	
	^(gameStage class = TicTacToeNotOver) not! !

!TicTacToe methodsFor: 'as yet unclassified' stamp: 'hh 5/31/2018 16:37:09'!
isPlayingO
	^turnState isPlayingO.! !

!TicTacToe methodsFor: 'as yet unclassified' stamp: 'hh 5/31/2018 16:37:14'!
isPlayingX
	^turnState isPlayingX.! !

!TicTacToe methodsFor: 'as yet unclassified' stamp: 'hh 5/31/2018 16:38:22'!
isTied
	
	^xs size = 5 and: [ os size = 4 and: [ self XHasWon not ]]! !

!TicTacToe methodsFor: 'as yet unclassified' stamp: 'hh 5/31/2018 16:42:08'!
putOAt: aPosition
	gameStage hasFinished.
	turnState isOTurn.
	self errorIfOccupied: aPosition.
	os add: aPosition.
	(self hasWin: os)
		ifTrue: [ gameStage _ TicTacToeOWonStage new ]
		ifFalse: [ turnState _ TicTacToeXTurn new ].! !

!TicTacToe methodsFor: 'as yet unclassified' stamp: 'hh 5/31/2018 16:42:17'!
putXAt: aPosition
	gameStage hasFinished.
	turnState isXTurn.
	self errorIfOccupied: aPosition.
	xs add: aPosition.
	(self hasWin: xs)
		ifTrue: [ gameStage _ TicTacToeXWonStage new ]
		ifFalse: [ turnState _ TicTacToeOTurn new ].
	self isTied ifTrue: [ gameStage _ TicTacToeTieStage new ].! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'TicTacToe class' category: #TicTacToStep21!
TicTacToe class
	instanceVariableNames: ''!

!TicTacToe class methodsFor: 'as yet unclassified' stamp: 'jg 5/22/2018 13:15:31'!
canNotPlayWhenGameIsOverErrorMessage
	
	^'Can not play when game is over'! !

!TicTacToe class methodsFor: 'as yet unclassified' stamp: 'jg 5/22/2018 10:26:38'!
notXTurnErrorMessage

	^'Not X turn'! !

!TicTacToe class methodsFor: 'as yet unclassified' stamp: 'jg 5/22/2018 11:20:12'!
positionTakenErrorMessage
	
	^'Position taken'! !


!classDefinition: #TicTacToeGameStage category: #TicTacToStep21!
Object subclass: #TicTacToeGameStage
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TicTacToStep21'!

!TicTacToeGameStage methodsFor: 'as yet unclassified' stamp: 'hh 5/31/2018 16:34:38'!
OHasWon

	^ self subclassResponsibility.! !

!TicTacToeGameStage methodsFor: 'as yet unclassified' stamp: 'hh 5/31/2018 16:10:04'!
XHasWon
	
	^self subclassResponsibility.! !

!TicTacToeGameStage methodsFor: 'as yet unclassified' stamp: 'hh 5/31/2018 16:09:23'!
hasFinished
	
	^self subclassResponsibility.! !


!classDefinition: #TicTacToeNotOver category: #TicTacToStep21!
TicTacToeGameStage subclass: #TicTacToeNotOver
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TicTacToStep21'!

!TicTacToeNotOver methodsFor: 'as yet unclassified' stamp: 'hh 5/31/2018 16:43:00'!
OHasWon
	
	^false.! !

!TicTacToeNotOver methodsFor: 'as yet unclassified' stamp: 'hh 5/31/2018 16:43:13'!
XHasWon
	
	^false.! !

!TicTacToeNotOver methodsFor: 'as yet unclassified' stamp: 'hh 5/31/2018 16:43:17'!
hasFinished
	
	^false.! !


!classDefinition: #TicTacToeOWonStage category: #TicTacToStep21!
TicTacToeGameStage subclass: #TicTacToeOWonStage
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TicTacToStep21'!

!TicTacToeOWonStage methodsFor: 'as yet unclassified' stamp: 'hh 5/31/2018 16:43:26'!
OHasWon
	
	^true.! !

!TicTacToeOWonStage methodsFor: 'as yet unclassified' stamp: 'hh 5/31/2018 16:43:30'!
XHasWon
	
	^false.! !

!TicTacToeOWonStage methodsFor: 'as yet unclassified' stamp: 'hh 5/31/2018 16:46:12'!
hasFinished

	self error: TicTacToe canNotPlayWhenGameIsOverErrorMessage.! !


!classDefinition: #TicTacToeTieStage category: #TicTacToStep21!
TicTacToeGameStage subclass: #TicTacToeTieStage
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TicTacToStep21'!

!TicTacToeTieStage methodsFor: 'as yet unclassified' stamp: 'hh 5/31/2018 16:43:40'!
OHasWon
	
	^false.! !

!TicTacToeTieStage methodsFor: 'as yet unclassified' stamp: 'hh 5/31/2018 16:43:44'!
XHasWon
	
	^false.! !

!TicTacToeTieStage methodsFor: 'as yet unclassified' stamp: 'hh 5/31/2018 16:46:18'!
hasFinished

	self error: TicTacToe canNotPlayWhenGameIsOverErrorMessage.! !


!classDefinition: #TicTacToeXWonStage category: #TicTacToStep21!
TicTacToeGameStage subclass: #TicTacToeXWonStage
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TicTacToStep21'!

!TicTacToeXWonStage methodsFor: 'as yet unclassified' stamp: 'hh 5/31/2018 16:47:50'!
OHasWon
	
	^false.! !

!TicTacToeXWonStage methodsFor: 'as yet unclassified' stamp: 'hh 5/31/2018 16:47:08'!
XHasWon
	
	^true.! !

!TicTacToeXWonStage methodsFor: 'as yet unclassified' stamp: 'hh 5/31/2018 16:45:59'!
hasFinished

	self error: TicTacToe canNotPlayWhenGameIsOverErrorMessage.! !


!classDefinition: #TicTacToeTurn category: #TicTacToStep21!
Object subclass: #TicTacToeTurn
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TicTacToStep21'!

!TicTacToeTurn methodsFor: 'as yet unclassified' stamp: 'LR 5/31/2018 04:06:46'!
isPlayingO
	^ false.! !

!TicTacToeTurn methodsFor: 'as yet unclassified' stamp: 'LR 5/31/2018 04:06:43'!
isPlayingX
	^ false.! !


!classDefinition: #TicTacToeOTurn category: #TicTacToStep21!
TicTacToeTurn subclass: #TicTacToeOTurn
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TicTacToStep21'!

!TicTacToeOTurn methodsFor: 'as yet unclassified' stamp: 'hh 5/31/2018 16:33:11'!
isOTurn! !

!TicTacToeOTurn methodsFor: 'as yet unclassified' stamp: 'LR 5/31/2018 06:28:57'!
isPlayingO
	^ true.! !

!TicTacToeOTurn methodsFor: 'as yet unclassified' stamp: 'LR 5/31/2018 06:14:43'!
isXTurn
	 [self error: TicTacToe notXTurnErrorMessage] value.! !


!classDefinition: #TicTacToeXTurn category: #TicTacToStep21!
TicTacToeTurn subclass: #TicTacToeXTurn
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TicTacToStep21'!

!TicTacToeXTurn methodsFor: 'as yet unclassified' stamp: 'hh 5/31/2018 16:12:27'!
isOTurn
	self error: TicTacToe notXTurnErrorMessage.! !

!TicTacToeXTurn methodsFor: 'as yet unclassified' stamp: 'LR 5/31/2018 05:09:49'!
isPlayingX
	^ true.! !

!TicTacToeXTurn methodsFor: 'as yet unclassified' stamp: 'hh 5/31/2018 16:11:36'!
isXTurn! !
