!classDefinition: #I category: #peano!
DenotativeObject subclass: #I
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'peano'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'I class' category: #peano!
I class
	instanceVariableNames: 'next previous'!

!I class methodsFor: 'as yet unclassified' stamp: 'as 3/26/2018 21:35:25'!
* unNumeroDePeano
	^unNumeroDePeano! !

!I class methodsFor: 'as yet unclassified' stamp: 'as 3/26/2018 21:35:48'!
+ unNumeroDePeano
	^unNumeroDePeano next.! !

!I class methodsFor: 'as yet unclassified' stamp: 'as 3/26/2018 21:38:05'!
- unNumeroDePeano
	self error: self descripciondeErrorDeNumerosNegativosNoSoportados.! !

!I class methodsFor: 'as yet unclassified' stamp: 'as 3/26/2018 21:39:59'!
/ unNumeroDePeano
	self error: self descripcionDeErrorDeNoSePuedeDividirPorUnNumeroMayor! !

!I class methodsFor: 'as yet unclassified' stamp: 'as 3/26/2018 21:14:39'!
INoTienePrevious
	self shouldBeImplemented.! !

!I class methodsFor: 'as yet unclassified' stamp: 'sad 3/30/2018 17:43:15'!
menos: unNumeroDePeano
	^unNumeroDePeano previous.! !

!I class methodsFor: 'as yet unclassified' stamp: 'as 3/26/2018 21:22:32'!
next
	next ifNil: [next:= self cloneNamed:(self name, 'I')].
	^next! !

!I class methodsFor: 'as yet unclassified' stamp: 'as 3/26/2018 21:13:17'!
previous
	self error: self INoTienePrevious! !


!I class methodsFor: 'error' stamp: 'as 3/26/2018 21:40:27'!
descripcionDeErrorDeNoSePuedeDividirPorUnNumeroMayor
	^'Error'! !

!I class methodsFor: 'error' stamp: 'hjgg 3/29/2018 08:21:54'!
descripciondeErrorDeNumerosNegativosNoSoportados
	^ 'No hay soporte de numeros negativos'.! !

I instVarNamed: 'next' put: nil!
I instVarNamed: 'previous' put: nil!

!classDefinition: #II category: #peano!
DenotativeObject subclass: #II
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'peano'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'II class' category: #peano!
II class
	instanceVariableNames: 'next previous'!

!II class methodsFor: 'as yet unclassified' stamp: 'asdada 3/28/2018 19:43:18'!
* unNumeroDePeano
	^(self previous * unNumeroDePeano) + unNumeroDePeano! !

!II class methodsFor: 'as yet unclassified' stamp: 'asdada 3/28/2018 19:42:28'!
+ unNumeroDePeano
	^(self previous) + (unNumeroDePeano next) ! !

!II class methodsFor: 'as yet unclassified' stamp: 'sad 3/30/2018 17:43:53'!
- unNumeroDePeano
	^(unNumeroDePeano menos: self)! !

!II class methodsFor: 'as yet unclassified' stamp: 'asdada 3/28/2018 20:28:05'!
/ unNumeroDePeano! !

!II class methodsFor: 'as yet unclassified' stamp: 'as 3/26/2018 21:26:41'!
INoTienePrevious
	self shouldBeImplemented.! !

!II class methodsFor: 'as yet unclassified' stamp: 'sad 3/30/2018 17:42:57'!
menos: unNumeroDePeano
	^unNumeroDePeano previous - self previous.! !

!II class methodsFor: 'as yet unclassified' stamp: 'as 3/26/2018 21:30:58'!
next
	next ifNil: [
		next:= self cloneNamed:(self name, 'I').
		next previous:self
	].
	^next.! !

!II class methodsFor: 'as yet unclassified' stamp: 'as 3/26/2018 21:27:58'!
previous
	^previous! !

!II class methodsFor: 'as yet unclassified' stamp: 'as 3/26/2018 21:28:31'!
previous: prev
	previous:= prev.! !

II instVarNamed: 'next' put: III!
II instVarNamed: 'previous' put: I!

!classDefinition: #III category: #peano!
DenotativeObject subclass: #III
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'peano'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'III class' category: #peano!
III class
	instanceVariableNames: 'next previous'!

!III class methodsFor: 'as yet unclassified' stamp: 'sad 3/30/2018 17:44:08'!
* unNumeroDePeano
	^(self previous * unNumeroDePeano) + unNumeroDePeano! !

!III class methodsFor: 'as yet unclassified' stamp: 'sad 3/30/2018 17:44:08'!
+ unNumeroDePeano
	^(self previous) + (unNumeroDePeano next) ! !

!III class methodsFor: 'as yet unclassified' stamp: 'sad 3/30/2018 17:44:08'!
- unNumeroDePeano
	^(unNumeroDePeano menos: self)! !

!III class methodsFor: 'as yet unclassified' stamp: 'sad 3/30/2018 17:44:08'!
/ unNumeroDePeano! !

!III class methodsFor: 'as yet unclassified' stamp: 'sad 3/30/2018 17:44:08'!
INoTienePrevious
	self shouldBeImplemented.! !

!III class methodsFor: 'as yet unclassified' stamp: 'sad 3/30/2018 17:44:08'!
menos: unNumeroDePeano
	^unNumeroDePeano previous - self previous.! !

!III class methodsFor: 'as yet unclassified' stamp: 'sad 3/30/2018 17:44:08'!
next
	next ifNil: [
		next:= self cloneNamed:(self name, 'I').
		next previous:self
	].
	^next.! !

!III class methodsFor: 'as yet unclassified' stamp: 'sad 3/30/2018 17:44:08'!
previous
	^previous! !

!III class methodsFor: 'as yet unclassified' stamp: 'sad 3/30/2018 17:44:08'!
previous: prev
	previous:= prev.! !

III instVarNamed: 'next' put: nil!
III instVarNamed: 'previous' put: II!